# RadioPy

This project helps your to build internet radio with the help of Raspberry Pi.

## Getting Started

The code written is extracted from PyRadio python module developed by some awesome folks and it's details available at [Github](https://github.com/coderholic/pyradio)  & [Project website](http://www.coderholic.com/pyradio/).

However the PyRadio is console based internet radio and requires to have console access to select radio stations or just to move to previous and next stations. Hence decided to write a fresh code to make use of push buttons and display.

This helped to have Internet Radio which can be played without use of remote login to pi. There are 3 push button which are used for previous and next stations and mute audio (This has not yet coded yet). Display show information like currently played station, IP address of pi, date and time (refreshed every 5 sec).

### Prerequisites

Raspberry Pi preparation:

* Python 2.7 & pip
* nodejs & npm -> pm2
* vlc (and check if cvlc command working)

Material:

* Raspberry Pi (2 in this case)
* wifi dongle
* 3 push buttons
* SPI display (in this case Nokia 5110 LCD is used)

```
Images here
```

### Installing

#### Preparation of Raspberry Pi

Attach wifi dongle to Raspberry Pi and make sure pi is connected to router and internet is accessible. If you are facing any issue take help from many threads in stachexchange such as [this](https://raspberrypi.stackexchange.com/questions/10251/prepare-sd-card-for-wifi-on-headless-pi)

Python code makes use of command for vlc player (cvlc) and its syntax. cvlc accepts station url as a parameter to play audio. Hence vlc need to be installed on raspberry pi and make sure [cvlc command](https://wiki.videolan.org/VLC_command-line_help/) executed properly and is playing url. Visit this link (https://wiki.videolan.org/VLC_command-line_help/) to see further details about cvlc command.

Additionally you need to install RPi.GPIO, python-imaging and Adafruit_Nokia_LCD using below statements.

To install python pip and install RPi.GPIO, python-imaging use below statements

```
sudo apt-get update
sudo apt-get install python-pip python-dev build-essential python-imaging git
```

to install GPIO

```
sudo pip install RPi.GPIO
```

Additionally you need to enable Hardware SPI on Raspberry Pi by executing below command 
```
sudo nano /boot/config.txt
```
and uncomment (remove the #) dtparam=spi=on , use ctrl+x to same changes and reboot raspberry pi to take effect. 
These instructions are taken from (https://www.algissalys.com/how-to/nokia-5110-lcd-on-raspberry-pi) url so you can refer the same if something is not understood.

To run python script in backround and run forever PM2 node package is used for which nodejs and npm need to be installed and PM2 need to be installed with help of below command.


```
npm i pm2
```

Refer to below connectivity diagram/url to make connections to display and push buttons. Note down which button you are using for previous, next and mute button and accordingly place them on enclosure. I made use of ready made PCBs to make hat for pi's GPIO and wires to extend connections to LCD and push button. Use hot glue as insulation and it will fix positions of wires and to give strength to joints.

I refered to (https://www.algissalys.com/how-to/nokia-5110-lcd-on-raspberry-pi) url for Nokia LCD connection diagram and placing image below for reference.

```
Image here
```

For push buttons simply used GPIO17 and GPIO27 pins hence final diagram is like below.

```
All pins connection diagram
```

Connect and solder long wires on PCB so it will be easy to fix buttons and display on enclosure.

I designed and 3D printed enclosure myself but you can use any enclosure which is readily available in market. Once everything is eclosed in box, place buttons and display on top or side wall. 

Update your favourite radio streaming stations in stations.csv file by adding lines in below format.

```
Vividh Bharati, http://vividhbharati-lh.akamaihd.net/i/vividhbharati_1@507811/index_1_a-p.m3u8
RED FM (93.5), http://14013.live.streamtheworld.com:3690/CKYRFM_SC
FM Gold, http://airfmgold-lh.akamaihd.net/i/fmgold_1@507591/master.m3u8
```

Now you are ready to play some music, attach external speaker to raspberry pi and enter below command

```
python ./radiopy.py
```

The script shoul run without any issue and start playing random radio station from your playlist (stations.csv). 
Make python script executable using below command.

```
sudo chmod +x radiopy.py
```

This should run radiopy.py file without needing put python in command. If you have followed readme line by line we have installed pm2 node package to run this command in background and same package will help us to start script at start. Run below commands to achieve the same.

to start radiopy under pm2 

```
pm2 start radiopy.py
```

to save currently running process names in pm2

```
pm2 save
```

to run pm2 processes at start use below command, this command will output one more command which you need to copy and execute in command line.

```
pm2 startup
```

These statements will help us setup radiopy as service under pm2 and will run at every startup. Now reboot pi to check the same and see if radio plays music without need to access to command line.

if not, there is PM2 site to help you with troubleshooting, head over to (http://pm2.keymetrics.io/)

This is end of this readme. Enjoy your own Internet radio based on Raspberry Pi. 

## Built With

* [Raspberry Pi](https://www.raspberrypi.org/products/) - The Engine
* [Python](https://www.python.org/) - The programming language
* [Adafruit Nokia LCD Library](https://github.com/adafruit/Adafruit_Nokia_LCD) - To enable display

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://confluence.atlassian.com/bitbucket/use-repository-tags-321860179.html). 

## Authors

* **Mandar Oak** - *Initial work* - [manddar](https://bitbucket.org/manddar/)

See also the list of [contributors](https://bitbucket.org/manddar/) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [PyRadio](https://github.com/coderholic/pyradio)
* [Adafruit Nokia LCD](https://github.com/adafruit/Adafruit_Nokia_LCD)
* [Algis Salys](https://www.algissalys.com/how-to/nokia-5110-lcd-on-raspberry-pi) - Used this post for display connections.

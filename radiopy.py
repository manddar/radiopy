#!/usr/bin/python

# All imports
import subprocess
import csv
import random
from os import path, getenv
import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library
import time
import socket
import threading

#Import for display
import Adafruit_Nokia_LCD as LCD
import Adafruit_GPIO.SPI as SPI
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

#Initialization for display
# Raspberry Pi hardware SPI config:
DC = 23
RST = 24
SPI_PORT = 0
SPI_DEVICE = 0
# Hardware SPI usage:
disp = LCD.PCD8544(DC, RST, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE, max_speed_hz=4000000))
# Initialize library.
disp.begin(contrast=60)

#Global variables are updated from functions
stations = []
random_station = []
selection = 0
station_name = ''
station_url = ''
# variable to manage debouncing
oldtime = time.time()

#function to check debouncing
def is_sec():
    global oldtime
    if time.time() - oldtime > 1.5:
        oldtime = time.time()
        return True
    else:
        return False

def get_IP():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip_address = s.getsockname()[0]
    s.close()
    return ip_address

def update_time_display():
    threading.Timer(5.0, update_time_display).start()
    display_text()

def display_text():
    # Clear display.
    disp.clear()
    disp.display()
    # Create blank image for drawing.
    # Make sure to create image with mode '1' for 1-bit color.
    image = Image.new('1', (LCD.LCDWIDTH, LCD.LCDHEIGHT))
    # Get drawing object to draw on image.
    draw = ImageDraw.Draw(image)
    # Draw a white filled box to clear the image.
    draw.rectangle((0,0,LCD.LCDWIDTH,LCD.LCDHEIGHT), outline=255, fill=255)
    # Load default font.
    font = ImageFont.load_default() 
    # Alternatively load a TTF font.
    # Some nice fonts to try: http://www.dafont.com/bitmap.php
    #font8 = ImageFont.truetype('slkscr.ttf', 8)
    # Write some text.
    draw.text((19,0), 'RadioPy', font=font)
    draw.text((0,12), stations[selection][0], font=font)
    draw.text((0,24), get_IP(), font=font)
    draw.text((0,38), time.strftime('%d/%m'), font=font)
    draw.text((55,38), time.strftime('%H:%M'), font=font)
    # Display image.
    disp.image(image)
    disp.display()

#Function to load stations from stations.csv file
def load_stations():
    p = path.join(path.dirname(__file__), 'stations.csv')
    if path.exists(p) and path.isfile(p):
        DEFAULT_FILE = p
    stations = []
    with open(p, 'r') as cfgfile:
            for row in csv.reader(filter(lambda row: row[0]!='#', cfgfile), skipinitialspace=True):
                if not row:
                    continue
                name, url = [s.strip() for s in row]
                stations.append((name, url))
    return stations

#Get random station to play for first time
def get_random_station():
    selection = random.randint(0, len(stations)-1)
    return selection

#Play any station passed to this function
def play_station():
    global process
    global stations
    global selection
    station_name = stations[selection][0]
    station_url = stations[selection][1].strip()
    opts = ["cvlc", "--quiet", station_url]
    process = subprocess.Popen(opts, shell=False, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
    display_text()
    print station_name 

def next_button_callback(channel):
    global process
    global stations
    global selection
    global oldtime
    if is_sec():
        process.kill()
        if selection == len(stations)-1 :
            selection = 0
        else:
            selection = selection + 1
        play_station()

def prev_button_callback(channel):
    global process
    global stations
    global selection
    global oldtime
    if is_sec():
        process.kill()
        if selection == 0:
            selection = len(stations) - 1
        else:
            selection = selection - 1
        play_station()

def setup_gpio():
    GPIO.setwarnings(False) # Ignore warning for now
    GPIO.setmode(GPIO.BCM) # Use BCM pin numbering
    GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # Set pin 10 to be an input pin and set initial value to be pulled low (off)
    GPIO.add_event_detect(17,GPIO.FALLING,callback=next_button_callback) # Setup event on pin 17 falling edge
    GPIO.setup(27, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # Set pin 10 to be an input pin and set initial value to be pulled low (off)
    GPIO.add_event_detect(27,GPIO.FALLING,callback=prev_button_callback) # Setup event on pin 27 falling edge

def main():
    global stations
    global selection
    setup_gpio()
    stations = load_stations()
    selection = get_random_station()
    play_station()
    update_time_display()
    a_input = input('Press Ctrl-C to quit.')

main()
